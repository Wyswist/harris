set(pathToProject /home/bartlomiej/PRACA/PROJEKTY/Android/Harris)
set(pathToOpenCv /home/bartlomiej/PRACA/opencv-3.3.0_Android)

cmake_minimum_required(VERSION 3.4.1)

set(CMAKE_VERBOSE_MAKEFILE on)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

include_directories(
                    src/main/cpp/include/
                    src/main/cpp/
                    src/main/jniLibs/arm64-v8a
                    )

include_directories(/home/bartlomiej/PRACA/opencv-3.3.0_Android/sdk/native/jni/include)

add_library( native-lib

             SHARED

             src/main/cpp/native-lib.cpp
             src/main/cpp/source/Harris.cpp
            )

add_library( lib_opencv SHARED IMPORTED )

set_target_properties(lib_opencv PROPERTIES IMPORTED_LOCATION ${pathToProject}/app/src/main/jniLibs/${ANDROID_ABI}/libopencv_java3.so)

find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

target_link_libraries( native-lib ${log-lib} lib_opencv ${pathToProject}/app/src/main/jniLibs/${ANDROID_ABI}/libopencv_ximgproc.a)