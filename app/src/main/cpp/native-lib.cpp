#include <jni.h>
#include <string>
#include <syslog.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include "include/Harris.h"

using namespace cv;
using namespace std;

extern "C"{

Harris harris;


JNIEXPORT void JNICALL
    Java_com_example_bartlomiej_harris_MainActivity_getFrame(JNIEnv *env, jclass type,
                                                         jlong nativeObjAdd) {
    Mat* globalFrame =  (Mat*)nativeObjAdd;
    resize(*globalFrame, *globalFrame, Size(), 0.5, 0.5);
    harris.src = globalFrame;
    harris.findFeatures();
    resize(*globalFrame, *globalFrame, Size(), 2, 2);
}


    JNIEXPORT void JNICALL
    Java_com_example_bartlomiej_harris_MainActivity_rotateImage(JNIEnv *env, jclass type,
                                                                jlong nativeObjAddSrc) {
        Mat* source = (Mat*)nativeObjAddSrc;
        Point2f src_center(source->cols/2.0F, source->rows/2.0F);
        Mat rot_mat = getRotationMatrix2D(src_center, 90, -1.0);
        warpAffine(*source, *source, rot_mat, source->size());
    }

}