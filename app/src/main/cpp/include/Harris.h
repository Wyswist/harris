//
// Created by bartlomiej on 21.02.18.
//

#ifndef HARRIS_HARRIS_H
#define HARRIS_HARRIS_H

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

class Harris {

public:

    Mat* src;
    Mat src_gray;
    int thresh = 120;

    int max_thresh = 255;
    const char* source_window = "Source image";
    const char* corners_window = "Corners detected";
    void cornerHarris_demo();
    void findFeatures();
};


#endif //HARRIS_HARRIS_H
