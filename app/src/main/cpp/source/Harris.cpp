//
// Created by bartlomiej on 21.02.18.
//

#include "include/Harris.h"

void Harris::cornerHarris_demo() {
//    Mat* pointer = (Mat*)ptr;
//    cvtColor( *pointer, *pointer, COLOR_BGR2GRAY );
    Mat dst, dst_norm, dst_norm_scaled;
    int counter= 0;
    dst = Mat::zeros( src->size(), CV_32FC1 );
    int blockSize = 6;
    int apertureSize = 3;
    double k = 0.04;
    cornerHarris( src_gray, dst, blockSize, apertureSize, k, BORDER_DEFAULT );
    normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
    convertScaleAbs( dst_norm, dst_norm_scaled );
    for( int j = 0; j < dst_norm.rows ; j++ )
    {
        for( int i = 0; i < dst_norm.cols; i++ )
        {
            if( (int) dst_norm.at<float>(j,i) > thresh )
            {
                circle( *src, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
//                circle( dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
                counter++;
                if(counter>20)
                    return;
            }
        }
    }
}

void Harris::findFeatures() {
    //  CommandLineParser parser( argc, argv, "{@input | ../data/building.jpg | input image}" );
    if ( src->empty() )
    {
        return;
    }
    cvtColor( *src, src_gray, COLOR_BGR2GRAY );
    cornerHarris_demo();
}
